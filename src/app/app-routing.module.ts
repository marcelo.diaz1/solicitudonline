import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CorreosComponent } from './correos/correos.component';
import { InicioComponent } from './inicio/inicio.component';
import { LoginComponent } from './login/login.component';
import { TicketsComponent } from './tickets/tickets.component';

const routes: Routes = [
  { path: 'inicio', component: InicioComponent },
  { path: 'correos', component: CorreosComponent },
  { path: 'tickets', component: TicketsComponent }, 
  { path: '', component: LoginComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
